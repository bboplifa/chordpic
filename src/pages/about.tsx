import * as React from 'react'
import { Row, Col } from 'react-amazing-grid'

import IndexLayout from '../layouts'
import styled from '@emotion/styled'

const CenteredRow = styled(Row)`
  max-width: 60rem;
`

const NotFoundPage = () => (
  <IndexLayout>
    <CenteredRow>
      <Col>
        <h1>About</h1>
        <p>ChordPic is a completely free tool to create guitar chord charts. </p>
        <p>While many tools exist to create guitar chord charts, ChordPic is by far the fastest and easiest solution.</p>
        <h2>Contribute</h2>
        <p>
          ChordPic is completely open source.{' '}
          <a href="https://gitlab.com/Voellmy/chordpic">The code of this website is available on GitLab</a>. If you are a developer and want
          to improve this web site, please feel free to create a pull request.
        </p>
        <h2>Feature Requests or Bug Reports</h2>
        If you're missing an essential feature or found a critical bug,{' '}
        <a href="https://gitlab.com/Voellmy/chordpic/issues">please create a ticket on GitLab</a>.
      </Col>
    </CenteredRow>
  </IndexLayout>
)

export default NotFoundPage
