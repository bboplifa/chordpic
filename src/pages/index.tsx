import * as React from 'react'
import { useEffect, useRef, useState } from 'react'
import { produce } from 'immer'
import IndexLayout from '../layouts'
import { ChordEditor } from '../components/ChordEditor'
import { ChordResult } from '../components/ChordResult'
import constants from '../constants'
import { Col, Row } from 'react-amazing-grid'

import DownloadButtons from '../components/DownloadButtons'

/**
 * Padding of the container
 */
const PADDING = 15

/**
 * Maximum width of the chord chart
 */
const MAX_WIDTH = 400

interface IChordSettings {
  name: string
  numFrets: number
  position: number
  tunings: string[]
}

interface IChordState {
  vexchord: IVexchord
  settings: IChordSettings
}

const defaultChordSettings: IChordSettings = {
  name: '',
  numFrets: 4,
  position: 1,
  tunings: Array(constants.numStrings).fill('')
}

const useResizeHandler = () => {
  let screenWidth = MAX_WIDTH

  if (typeof window !== `undefined`) {
    screenWidth = window.screen.availWidth
  }

  const [width, setWidth] = useState<number>(Math.min(screenWidth, MAX_WIDTH - PADDING * 2))

  useEffect(() => {
    const handleResize = () => {
      window.requestAnimationFrame(() => setWidth(screenWidth))
    }

    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  })

  const finalWidth = Math.min(width, MAX_WIDTH) - PADDING * 2

  return { width: finalWidth, height: finalWidth * 1.5 }
}

const IndexPage = () => {
  const chartRef = useRef<HTMLDivElement | null>(null)
  const [state, setState] = useState<IChordState>({
    vexchord: { barres: [], chords: [] },
    settings: defaultChordSettings
  })

  const { width, height } = useResizeHandler()

  const {
    vexchord,
    settings: { name, numFrets, position, tunings }
  } = state

  return (
    <IndexLayout>
      <Row>
        <Col>
          <h2>Editor</h2>
          <ChordEditor
            name={name}
            width={width * 0.75}
            height={height * 0.6}
            numFrets={numFrets}
            position={position}
            tunings={tunings}
            onTunings={newTunings =>
              setState(
                produce(state, draft => {
                  draft.settings.tunings = newTunings
                })
              )
            }
            onVexchord={newVexchord => setState({ ...state, vexchord: newVexchord })}
            onName={newName =>
              setState(
                produce(state, draft => {
                  draft.settings.name = newName
                })
              )
            }
            onNumFrets={newNumFrets =>
              newNumFrets && newNumFrets > 0
                ? setState(
                    produce(state, draft => {
                      draft.settings.numFrets = newNumFrets
                    })
                  )
                : null
            }
            onPosition={newPosition =>
              setState(
                produce(state, draft => {
                  draft.settings.position = newPosition
                })
              )
            }
          />
        </Col>
        <Col>
          <h2>Result</h2>
          <ChordResult
            vexchord={vexchord}
            chartRef={chartRef}
            name={name}
            numFrets={numFrets}
            position={position}
            tunings={tunings}
            width={width}
            height={height}
          />
          <DownloadButtons chartRef={chartRef} />
        </Col>
      </Row>
    </IndexLayout>
  )
}

export default IndexPage
