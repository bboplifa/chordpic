import * as React from 'react'
import { Link } from 'gatsby'

import IndexLayout from '../layouts'
import { Row } from 'react-amazing-grid'

const NotFoundPage = () => (
  <IndexLayout>
    <Row>
      <h1>404: Page not found.</h1>
      <p>
        You've hit the void. <Link to="/">Go back.</Link>
      </p>
    </Row>
  </IndexLayout>
)

export default NotFoundPage
