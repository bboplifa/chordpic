import * as React from 'react'
import { Row, Col } from 'react-amazing-grid'

import IndexLayout from '../layouts'
import styled from '@emotion/styled'
import toggleGif from '../images/toggle.gif'
import silentstringsGif from '../images/silentstrings.gif'
import barrechordGif from '../images/barre.gif'
import labelsGif from '../images/labels.gif'
import sampleChord from '../images/samplechord.svg'

const CenteredRow = styled(Row)`
  max-width: 60rem;
`

const ResponsiveImg = styled.img`
  max-width: 20rem;
  height: auto;
`

const NotFoundPage = () => (
  <IndexLayout>
    <CenteredRow>
      <Col>
        <h1>Help</h1>
        <p>
          If you haven't figured it out already, here's how you create a chord chart and then save it as a PNG or SVG image. Don't worry,
          it's super simple!
        </p>
        <p>
          ChordPic has 3 main sections: The <i>Editor</i>, the <i>Result</i> and the <i>Download</i> section.
        </p>
        <p>
          As the name suggests, in the editor section you edit you chord chart. Every change of the chord chart is done in this section!
          More on this section later.
        </p>
        <p>In the Result section you can see a preview of your chord chart.</p>
        <p>The download section allows you to download your chart in different formats.</p>

        <h2>The Editor</h2>

        <p>
          <strong>Adding / removing fingers</strong>: Simply click anywhere you want the finger to appear. To remove the finger, just click
          on it again and it will disappear.
        </p>

        <img src={toggleGif} alt="Example of adding and removing fingers" />

        <p>
          <strong>Toggling silent or open strings</strong>: If there is no finger on a string, an 'O' automatically appears above the string
          (open string). If you want to change that to an 'X' (don't play that string) simply click on the 'O' to make it an 'X'. When you
          click it again it will change back to an 'O'.
        </p>

        <ResponsiveImg src={silentstringsGif} alt="Example of toggling strings from do not play to open" />

        <p>
          <strong>Adding a barre chord</strong>: To add a barre chord, you can simply connect the strings with the mouse or if you're on
          mobile you can swipe from one string to another with your finger. To remove the bare chord simply click anywhere on the fret with
          the barre chord to remove it.
        </p>

        <ResponsiveImg src={barrechordGif} alt="Example of adding and removing a barre chord" />

        <p>
          <strong>Adding labels to strings</strong>: To label the strings you can enter any letters or numbers below the strings. By default
          the strings are not labelled.
        </p>

        <ResponsiveImg src={labelsGif} alt="Example of adding and removing a barre chord" />

        <p>
          <strong>Adding labels to strings</strong>: To label the strings you can enter any letters or numbers below the strings. By default
          the strings are not labelled.
        </p>

        <h2>The Result Section</h2>

        <p>
          The result section gives you a preview of what your chart image will look like. All changes made in the editor section are
          immediately visible in the result section. Sample chart:{' '}
        </p>

        <ResponsiveImg src={sampleChord} alt="Example chord chart" />

        <h2>The Download Section</h2>

        <p>
          In the download section you can download your chord chart as an image. You can export the image as an SVG or PNG image. When you
          download a PNG image, you have to chose between different resolutions.
        </p>
      </Col>
    </CenteredRow>
  </IndexLayout>
)

export default NotFoundPage
