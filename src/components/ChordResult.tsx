import * as React from 'react'
import { MutableRefObject, useEffect } from 'react'
import { ChordBox } from 'vexchords'

interface IProps {
  vexchord: IVexchord
  chartRef: MutableRefObject<HTMLDivElement | null>
  width: number
  height: number
  name: string
  numFrets: number
  position: number
  tunings: string[]
}

export const ChordResult = ({ vexchord, name, numFrets, position, tunings, width, height, chartRef }: IProps) => {
  useEffect(() => {
    if (chartRef.current) {
      chartRef.current.innerHTML = ''
    }

    const chordBox = new ChordBox('#chord-result', {
      // Customizations (all optional, defaults shown)
      width,
      height,

      circleRadius: 20, // circle radius (numStrings / 20 by default)
      strokeWidth: 3,

      numStrings: 6, // number of strings (e.g., 4 for bass)
      numFrets, // number of frets (e.g., 7 for stretch chords)
      showTuning: true, // show tuning keys

      defaultColor: '#000', // default color
      bgColor: '#FFF', // background color
      strokeColor: '#000', // stroke color (overrides defaultColor)
      textColor: '#000', // text color (overrides defaultColor)
      stringColor: '#000', // string color (overrides defaultColor)
      fretColor: '#000', // fret color (overrides defaultColor)
      labelColor: '#000', // label color (overrides defaultColor)

      position,

      fretWidth: 2, // fret numStrings
      stringWidth: 2, // string numStrings

      fontFamily: 'Arial, "Helvetica Neue", Helvetica, sans-serif'
      // fontSize,
      // fontWeight,
      // fontStyle, // font settings
      // labelWeight // weight of label font
    })

    chordBox.draw({
      name,

      // array of [string, fret, label (optional)]
      // chord: [
      //   [1, 2],
      //   [2, 1],
      //   [3, 2],
      //   [4, 2], // fret 0 = open string
      //   [5, 'x'], // fret x = muted string
      //   [6, 'x']
      // ],
      chord: vexchord.chords,

      // optional: position marker
      position,

      // optional: barres for barre chords
      barres: vexchord.barres,

      // optional: tuning keys
      tuning: tunings
    })
  })

  return <div id="chord-result" ref={chartRef} />
}
