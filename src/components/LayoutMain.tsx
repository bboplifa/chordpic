import * as React from 'react'
import { Container } from 'react-amazing-grid'

const LayoutMain: React.FC<{}> = ({ children }) => <Container>{children}</Container>

export default LayoutMain
