import * as React from 'react'
import styled from '../styles/theme'
import constants from '../constants'
import { IChordInputSettings } from './ChordEditor'

interface IProps {
  settings: IChordInputSettings
  tunings: string[]
  onTunings: (tunings: string[]) => void
}

const StyledTuningInput = styled.div<IChordInputSettings>`
  display: grid;
  width: ${props => props.width + props.circleSize / 2}px;
  grid-template-columns: repeat(${constants.numStrings}, 1fr);
  grid-row-gap: ${props => props.lineWidth}px;
  grid-template-rows: ${props => props.height / 4}px;

  .string-input {
    position: relative;
    padding: 1px;
  }

  input {
    width: 100%;
    text-align: center;
    font-size: 2rem;
    margin-top: 0.5rem;
  }
`

const ScreenReaderLabel = styled.label`
  position: absolute !important; /* Outside the DOM flow */
  height: 1px;
  width: 1px; /* Nearly collapsed */
  overflow: hidden;
  clip: rect(1px 1px 1px 1px); /* IE 7+ only support clip without commas */
  clip: rect(1px, 1px, 1px, 1px); /* All other browsers */
`

const Input = styled.input`
  border: 2px solid black;
  border-radius: 3px;
  padding: 0;
`

export default (props: IProps) => (
  <StyledTuningInput {...props.settings}>
    {props.tunings.map((tuning, i) => (
      <div key={i} className="string-input" data-cell-index={i}>
        <ScreenReaderLabel htmlFor={`tuning-input-string-${i}`}>Tuning of String ${i + 1}</ScreenReaderLabel>
        <Input
          id={`tuning-input-string-${i}`}
          placeholder={String(i + 1)}
          type="text"
          value={tuning}
          onChange={e => props.onTunings(props.tunings.map((val, j) => (i === j ? e.target.value : val)))}
        />
      </div>
    ))}
  </StyledTuningInput>
)
