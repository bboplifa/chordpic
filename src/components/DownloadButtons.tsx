import * as React from 'react'
import { ThemeProvider } from 'emotion-theming'
import { theme } from '../styles/theme'
import styled from '../styles/theme'
import { ImageService } from '../services/image-service'
import { MutableRefObject } from 'react'

const downloadPng = (chartDom: HTMLDivElement | null, widht = 400, height = 600) => () => {
  if (!chartDom) {
    return
  }

  const svg = chartDom.innerHTML
  ImageService.downloadPng(svg, widht, height)
}

const downloadSvg = (chartDom: HTMLDivElement | null) => () => {
  if (!chartDom) {
    return
  }

  const svg = chartDom.innerHTML
  ImageService.downloadSvg(svg)
}

const Button = styled.button`
  background-color: ${props => props.theme.colors.background};
  color: ${props => props.theme.colors.text};
  border: 2px solid ${props => props.theme.colors.text};
  padding: 0.5rem;
  border-radius: 3px;
  margin-right: 0.5rem;
  margin-bottom: 0.5rem;
  cursor: pointer;
`

interface IProps {
  chartRef: MutableRefObject<HTMLDivElement | null>
}

export const DownloadButtons = ({ chartRef }: IProps) => (
  <ThemeProvider theme={theme}>
    <h2>Download</h2>
    <Button onClick={downloadSvg(chartRef.current)}>SVG</Button>
    <Button onClick={downloadPng(chartRef.current)}>PNG (400 x 600)</Button>
    <Button onClick={downloadPng(chartRef.current, 800, 1200)}>PNG (800 x 1200)</Button>
    <Button onClick={downloadPng(chartRef.current, 1600, 2400)}>PNG (1600 x 2400)</Button>
  </ThemeProvider>
)

export default DownloadButtons
