import * as React from 'react'
import { ChangeEvent, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import { Input, Label } from '@rebass/forms'
import constants from '../constants'
import TuningInput from './TuningInput'
import ChordInput from './ChordInput'
import SilentStringsInput from './SilentStringsInput'
import { ChordMatrix } from '../services/chord-matrix'

const lineWidth = 3

export interface IChordInputSettings {
  width: number
  height: number
  lineWidth: number
  circleSize: number
}

const InputGroup = styled.div(
  () => `
  label {
    font-size: 1.2rem;
  }

  input {
    margin-bottom: 1rem;
    border: 2px solid black;
    width: 10rem;
  }
`
)

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;

  ${InputGroup} {
    margin-right: 1rem;
  }
`

interface IProps {
  name: string
  position: number
  numFrets: number
  tunings: string[]
  onName: (name: string) => void
  onVexchord: (vexchord: IVexchord) => void
  onNumFrets: (n: number) => void
  onPosition: (n: number) => void
  onTunings: (tuning: string[]) => void
  width: number
  height: number
}

export enum SilentState {
  X,
  O
}

export const ChordEditor = (props: IProps) => {
  const [matrix, setMatrix] = useState(new ChordMatrix(props.numFrets))
  useEffect(() => props.onVexchord(matrix.toVexchord()), [props.numFrets])

  const { name, position, numFrets, tunings, width, height } = props

  const onNameChange = (e: ChangeEvent<HTMLInputElement>) => (e.target ? props.onName(e.target.value) : null)
  const onPositionChange = (e: ChangeEvent<HTMLInputElement>) => e.target && props.onPosition(Number(e.target.value))
  const onNumFretsChange = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
    if (e.target && e.target.value && !isNaN(Number(e.target.value))) {
      const frets = Number(e.target.value)
      props.onNumFrets(frets)
      setMatrix(matrix.setNumFrets(frets))
    }
  }

  const onMatrixChange = (newMatrix: ChordMatrix) => {
    // callback
    props.onVexchord(newMatrix.toVexchord())

    setMatrix(newMatrix)
  }

  const circleSize = Math.min(50, width / constants.numStrings - 2)
  const settings = { lineWidth, circleSize, width, height }

  return (
    <>
      <Form>
        <InputGroup>
          <Label htmlFor="name">Name</Label>
          <Input id="name" name="name" onChange={onNameChange} value={name} />
        </InputGroup>
        <InputGroup>
          <Label htmlFor="start-at">Starting Fret</Label>
          <Input id="start-at" name="name" type="number" onChange={onPositionChange} placeholder={position} />
        </InputGroup>
        <InputGroup>
          <Label htmlFor="num-frets">Number of Frets</Label>
          <Input
            id="num-frets"
            name="num-frets"
            type="number"
            placeholder={numFrets}
            onChange={onNumFretsChange}
            max={constants.maxFrets}
            min="2"
          />
        </InputGroup>
      </Form>

      <SilentStringsInput settings={settings} matrix={matrix} onMatrixChange={onMatrixChange} />
      <ChordInput matrix={matrix} settings={settings} onMatrixChange={onMatrixChange} />
      <TuningInput settings={settings} tunings={tunings} onTunings={props.onTunings} />
    </>
  )
}
