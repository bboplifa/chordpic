import * as React from 'react'
import styled, { theme } from '../styles/theme'
import { ThemeProvider } from 'emotion-theming'
import { Menu } from 'react-feather'
import { useState } from 'react'
import { Link } from 'gatsby'

const Nav = styled.nav`
  display: flex;
  align-items: center;
  padding: 1rem 1rem 1rem 15px;
  background-color: ${props => props.theme.colors.text};
  color: ${props => props.theme.inverted.text};

  justify-content: space-between;
  flex-wrap: wrap;
`

const BrandLink = styled(Link)`
  font-size: 1.25rem;
  color: ${props => props.theme.inverted.link};
  text-decoration: none;
`

const IconButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;

  @media screen and (min-width: 600px) {
    display: none;
  }
`

const LinkList = styled.div<{ open: boolean }>`
  flex: 0 1 100%;
  transition: max-height 0.3s ease-in-out;
  max-height: ${props => (props.open ? '7rem' : '0')};

  ul {
    padding-inline-start: 0;
    list-style: none;

    > li {
      padding: 0.5rem;
    }
  }

  a {
    color: ${props => props.theme.inverted.link};
  }

  @media screen and (min-width: 600px) {
    flex: 0 1 auto;
    max-height: unset;

    ul > li {
      display: inline-block;
    }
  }
`

export const Header = () => {
  const [open, setOpen] = useState(false)

  return (
    <ThemeProvider theme={theme}>
      <Nav>
        <BrandLink to="/">ChordPic</BrandLink>
        <IconButton aria-hidden="true" onClick={() => setOpen(!open)}>
          <Menu color={theme.inverted.text} size={28} />
        </IconButton>

        <LinkList open={open}>
          <ul>
            <li>
              <Link to="/help">Help</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </LinkList>
      </Nav>
    </ThemeProvider>
  )
}

export default Header
