import * as React from 'react'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import 'modern-normalize'

import { css, Global } from '@emotion/core'
import CookieConsent from 'react-cookie-consent'
import Header from '../components/Header'
import LayoutRoot from '../components/LayoutRoot'
import LayoutMain from '../components/LayoutMain'
import { theme } from '../styles/theme'

interface StaticQueryProps {
  site: {
    siteMetadata: {
      title: string
      description: string
      keywords: string
    }
  }
}

const IndexLayout: React.FC = ({ children }) => (
  <StaticQuery
    query={graphql`
      query IndexLayoutQuery {
        site {
          siteMetadata {
            title
            description
            keywords
          }
        }
      }
    `}
    render={(data: StaticQueryProps) => (
      <LayoutRoot>
        <Global
          styles={css`
            body {
              font-family: ${theme.fonts.body};
            }
          `}
        />
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'keywords', content: data.site.siteMetadata.keywords },
            { name: 'description', content: data.site.siteMetadata.description }
          ]}
        >
          <html lang="en" />
        </Helmet>
        <Header />
        <LayoutMain>{children}</LayoutMain>
        <CookieConsent
          location="bottom"
          buttonText="OK"
          style={{ background: 'black' }}
          buttonStyle={{ color: 'black', fontSize: '13px', backgroundColor: 'white' }}
          expires={150}
        >
          This website uses cookies to enhance the user experience.
        </CookieConsent>
      </LayoutRoot>
    )}
  />
)

export default IndexLayout
