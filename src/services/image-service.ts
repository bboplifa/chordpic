import dayjs from 'dayjs'

const FILENAME_DATE_FORMAT = 'YYYY-MM-DD-mm-ss'

function svgString2Image(svgString: string, width: number, height: number): Promise<string> {
  return new Promise(resolve => {
    // SVG data URL from SVG string
    const svgData = `data:image/svg+xml;base64,${btoa(unescape(encodeURIComponent(svgString)))}`
    // create canvas in memory(not in DOM)
    const canvas = document.createElement('canvas')
    // get canvas context for drawing on canvas
    const context = canvas.getContext('2d')
    // set canvas size
    canvas.width = width
    canvas.height = height
    // create image in memory(not in DOM)
    const image = new Image()
    // later when image loads run this
    image.onload = () => {
      // async (happens later)
      if (!context) {
        throw new Error('Oops. Context was not defined.')
      }
      // clear canvas
      context.clearRect(0, 0, width, height)
      // draw image with SVG data to canvas
      context.drawImage(image, 0, 0, width, height)
      // snapshot canvas as png
      const pngData = canvas.toDataURL('image/png')
      // pass png data URL to callback
      resolve(pngData)
    } // end async
    // start loading SVG data into in memory image
    image.src = svgData
  })
}

export class ImageService {
  static async downloadPng(svgString: string, width: number, height: number, name = `chart-${dayjs().format(FILENAME_DATE_FORMAT)}.png`) {
    const base64png = await svgString2Image(svgString, width, height)

    // hack to trigger the download
    // const url = base64png.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
    const fakeLink = document.createElement('a')
    fakeLink.setAttribute('href', base64png)
    fakeLink.setAttribute('download', name)
    fakeLink.click()
  }

  static downloadSvg(svgString: string, name = `chart-${dayjs().format(FILENAME_DATE_FORMAT)}.svg`) {
    // / Create a fake <a> element
    const fakeLink = document.createElement('a')

    // hack to trigger the download
    fakeLink.setAttribute('href', `data:image/svg+xml;base64,${window.btoa(svgString)}`)
    fakeLink.setAttribute('download', name)
    fakeLink.click()
  }
}
