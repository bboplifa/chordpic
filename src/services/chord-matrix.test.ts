import { CellState, ChordMatrix, EmptyStringState } from './chord-matrix'

describe('Chord Matrix', () => {
  let matrix: ChordMatrix

  beforeEach(() => {
    matrix = new ChordMatrix(3, 3)
  })

  it('Toggles the state at the given position', () => {
    // when
    matrix.toggle(1, 1)

    // then
    expect(matrix.get(1, 1)).toEqual(CellState.ACTIVE)
  })

  it('Correctly computes emtpy string states', () => {
    // when
    matrix.toggle(1, 1)

    // then
    expect(matrix.getEmptyStringStates()).toEqual([EmptyStringState.O, EmptyStringState.NOT_EMPTY, EmptyStringState.O])
  })

  it('Should toggle the empty state', () => {
    // when
    matrix.toggleEmptyState(1)

    // then
    expect(matrix.getEmptyStringStates()[1]).toEqual(EmptyStringState.X)
  })

  it('Should correctly increase the numFrets', () => {
    // when
    matrix.setNumFrets(5)

    // then
    expect(matrix.rows).toHaveLength(5)
  })

  it('Should correctly decrease the numFrets', () => {
    // when
    matrix.setNumFrets(2)

    // then
    expect(matrix.rows).toHaveLength(2)
  })

  it('Should correcly render a chord array', () => {
    // when
    matrix.toggle(0, 0)
    matrix.toggle(1, 1)
    matrix.toggleEmptyState(2)

    matrix.print()

    // then
    expect(matrix.toChord()).toContainEqual([1, 'x'])
    expect(matrix.toChord()).toContainEqual([2, 2])
    expect(matrix.toChord()).toContainEqual([3, 1])
  })

  it('Should correctly set the state of connected cells', () => {
    // when
    matrix.connect(1, 0, 2)
    matrix.print()

    // then
    expect(matrix.rows[1]).toEqual([CellState.LEFT, CellState.MIDDLE, CellState.RIGHT])
  })

  it('Should correctly set the state of connected cells (reversed)', () => {
    // when
    matrix.connect(1, 2, 0)
    matrix.print()

    // then
    expect(matrix.rows[1]).toEqual([CellState.LEFT, CellState.MIDDLE, CellState.RIGHT])
  })

  it('Should correctly set the state of highlighted cells', () => {
    // when
    matrix.connectHighlight(1, 0, 2)
    matrix.print()

    // then
    expect(matrix.rows[1]).toEqual([CellState.LEFT_HL, CellState.MIDDLE_HL, CellState.RIGHT_HL])
  })

  it('Should correctly set the state of highlighted cells (reverse)', () => {
    // when
    matrix.connectHighlight(1, 2, 0)
    matrix.print()

    // then
    expect(matrix.rows[1]).toEqual([CellState.LEFT_HL, CellState.MIDDLE_HL, CellState.RIGHT_HL])
  })

  it('Should un-highlight cells on the same fret when highlighting other cells on the same fret', () => {
    // given
    matrix.connectHighlight(1, 0, 1)

    // when
    matrix.connectHighlight(1, 1, 2)
    matrix.print()

    // then
    expect(matrix.rows[1]).toEqual([CellState.INACTIVE, CellState.LEFT_HL, CellState.RIGHT_HL])
  })

  it('Should remove the connected cells when a strng is toggled on the same fret', () => {
    // given
    matrix.connect(1, 0, 2)

    // when
    matrix.toggle(1, 1)

    // then
    expect(matrix.rows[1]).toEqual([CellState.INACTIVE, CellState.ACTIVE, CellState.INACTIVE])
  })

  it('Should incativate all other strings when connecting on a fret', () => {
    // given
    matrix.toggle(2, 1)
    matrix.print()

    // when
    matrix.connect(1, 0, 1)
    matrix.print()

    // then
    expect(matrix.rows[1]).toEqual([CellState.LEFT, CellState.RIGHT, CellState.INACTIVE])
  })

  it('Should correctly render barre chords', () => {
    // given
    matrix.connect(1, 0, 2)

    // then
    expect(matrix.toBarres()).toEqual([
      {
        fromString: 3,
        toString: 1,
        fret: 2
      }
    ])
  })

  test.each`
    string | empty
    ${0}   | ${true}
    ${1}   | ${false}
    ${2}   | ${true}
  `('Computes correctly if string is empty', ({ string, empty }) => {
    matrix.toggle(1, 1)
    matrix.print()
    expect(matrix.isEmptyString(string)).toBe(empty)
  })
})
