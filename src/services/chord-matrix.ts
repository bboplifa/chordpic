import chunk from 'lodash.chunk'
import range from 'lodash.range'
import constants from '../constants'

export enum CellState {
  ACTIVE, // single finger
  INACTIVE, // single finger
  MIDDLE,
  LEFT,
  RIGHT,
  MIDDLE_HL,
  LEFT_HL,
  RIGHT_HL
}

export enum EmptyStringState {
  X,
  O,
  NOT_EMPTY
}

/**
 * This class contains all the logic of the chords. The state is held in a 1D array. Each mutating operation will
 * return a new instance.
 */
export class ChordMatrix {
  constructor(
    public numFrets: number,
    public readonly numStrings = constants.numStrings,
    private state: CellState[] = Array(numStrings * numFrets).fill(CellState.INACTIVE),
    private emptyStringsStates: EmptyStringState[] = Array(numStrings).fill(EmptyStringState.O)
  ) {}

  public clone() {
    return new ChordMatrix(this.numFrets, this.numStrings, this.state, this.emptyStringsStates)
  }

  setNumFrets(numFrets: number): ChordMatrix {
    if (numFrets < this.numFrets) {
      this.state = [...this.state].splice(0, this.numStrings * numFrets)
    } else if (numFrets > this.numFrets) {
      this.state = [...this.state, ...Array((numFrets - this.numFrets) * this.numStrings).fill(CellState.INACTIVE)]
    }

    this.numFrets = numFrets

    return this.clone()
  }

  get rows(): CellState[][] {
    return chunk(this.state, this.numStrings)
  }

  get strings(): CellState[][] {
    return this.state.reduce<CellState[][]>(
      (strings, s, i) => {
        strings[i % this.numStrings].push(s)

        return strings
      },
      Array(this.numStrings)
        .fill(0)
        .map(() => [])
    )
  }

  get(fret: number, string: number): CellState {
    return this.state[fret * this.numStrings + string]
  }

  private set(string: number, fret: number, state: CellState): ChordMatrix {
    this.state[this.getIndex(string, fret)] = state

    return this.clone()
  }

  toggleEmptyState(string: number): ChordMatrix {
    this.emptyStringsStates[string] = this.emptyStringsStates[string] === EmptyStringState.O ? EmptyStringState.X : EmptyStringState.O

    return this.clone()
  }

  isEmptyString(string: number) {
    return !this.strings[string].some(s => s !== CellState.INACTIVE)
  }

  getEmptyStringStates(): EmptyStringState[] {
    return range(0, this.numStrings).map(i => (this.isEmptyString(i) ? this.emptyStringsStates[i] : EmptyStringState.NOT_EMPTY))
  }

  toggle(string: number, fret: number): ChordMatrix {
    console.log(`set string ${string} and fret ${fret}`)

    // remove all barre chords from this fret
    this.fretIndices(fret).forEach(i => {
      const state = this.state[i]
      if ([CellState.MIDDLE, CellState.LEFT, CellState.RIGHT].includes(state)) {
        this.state[i] = CellState.INACTIVE
      }
    })

    const currentState = this.state[this.getIndex(string, fret)]
    return this.set(string, fret, currentState !== CellState.INACTIVE ? CellState.INACTIVE : CellState.ACTIVE)
  }

  connect(fret: number, fromString: number, toString: number): ChordMatrix {
    console.log(`Connect from ${fromString} to ${toString} at fret ${fret}`)

    this.clearFret(fret)
    this.drawBarre(fret, fromString, toString)
    return this.clone()
  }

  connectHighlight(fret: number, fromString: number, toString: number) {
    this.clearFret(fret)
    this.drawBarre(fret, fromString, toString, true)
    return this.clone()
  }

  clearHighlight() {
    this.state = this.state.map(s => ([CellState.LEFT_HL, CellState.RIGHT_HL, CellState.MIDDLE_HL].includes(s) ? CellState.INACTIVE : s))
    return this.clone()
  }

  private clearFret(fret: number) {
    this.fretIndices(fret).forEach(i => {
      this.state[i] = CellState.INACTIVE
    })
  }

  private drawBarre(fret: number, fromString: number, toString: number, highlight = false) {
    const from = Math.min(fromString, toString)
    const to = Math.max(fromString, toString)

    // input validation
    if (fret > this.numFrets - 1) {
      throw new Error(`Fret ${fret} is out of range.`)
    }
    if (Math.abs(from - to) < 1) {
      throw new Error('Strings must be at least 1 apart from each other')
    }
    if (from < 0 || from >= this.numStrings) {
      throw new Error(`fromString is out of range`)
    }
    if (to < 0 || to >= this.numStrings) {
      throw new Error(`toString is out of range`)
    }

    // create the barre chord
    range(from, to + 1).forEach((string, count) => {
      let state: CellState

      if (count === 0) {
        state = highlight ? CellState.LEFT_HL : CellState.LEFT
      } else if (count === to - from) {
        state = highlight ? CellState.RIGHT_HL : CellState.RIGHT
      } else {
        state = highlight ? CellState.MIDDLE_HL : CellState.MIDDLE
      }

      this.state[this.getIndex(string, fret)] = state
    })
  }

  print() {
    const stateToAscii = (s: CellState): string => {
      switch (s) {
        case CellState.ACTIVE:
          return 'X'
        case CellState.INACTIVE:
          return ' '
        case CellState.RIGHT:
          return '>'
        case CellState.LEFT:
          return '<'
        case CellState.RIGHT_HL:
          return ')'
        case CellState.LEFT_HL:
          return '('
        case CellState.MIDDLE:
          return '='
        case CellState.MIDDLE_HL:
          return '-'
      }
    }

    const ascii = this.rows.reduce((acc, row) => {
      return `${acc}|${row.map(stateToAscii).join('|')}|\n`
    }, '')

    console.log(ascii)
  }

  /**
   * Transforms the internal representation of the cell states to to a form that is understood by vexchords.
   */
  toChord(): IChord[] {
    const newState = [...this.state]
      .splice(0, this.numFrets * this.numStrings)
      .reduce<IChord[]>(
        (acc, state, i) =>
          state === CellState.ACTIVE
            ? [...acc, [Math.abs((i % this.numStrings) - this.numStrings), Math.floor(i / this.numStrings + 1)]]
            : acc,
        []
      )

    const emptyStringStates = this.emptyStringIndices().map<[number, number | string]>(stringIndex => {
      return [Math.abs(stringIndex - this.numStrings), this.emptyStringsStates[stringIndex] === EmptyStringState.O ? 0 : 'x']
    })

    return [...newState, ...emptyStringStates]
  }

  /**
   * Transforms the internal representation of the cell states to to a form that is understood by vexchords.
   */
  toBarres(): IBarre[] {
    return this.state.reduce(
      (barres, state, i) => {
        const { fret, string } = this.translate(i)

        const vexString = Math.abs(string - this.numStrings)
        const vexFret = fret + 1

        if (state === CellState.LEFT) {
          barres = [...barres, { fromString: vexString, toString: vexString, fret: vexFret }]
        } else if (state === CellState.RIGHT) {
          barres[barres.length - 1].toString = vexString
        }

        return barres
      },
      [] as IBarre[]
    )
  }

  toVexchord(): IVexchord {
    return {
      chords: this.toChord(),
      barres: this.toBarres()
    }
  }

  private emptyStringIndices(): number[] {
    return range(0, this.numStrings).reduce((acc, string) => (this.isEmptyString(string) ? [...acc, string] : acc), [] as number[])
  }

  private getIndex(string: number, fret: number): number {
    return fret * this.numStrings + string
  }

  private fretIndices(fret: number): number[] {
    const start = fret * this.numStrings
    const end = start + this.numStrings
    return range(start, end)
  }

  translate(i: number): { string: number; fret: number } {
    return {
      string: i % this.numStrings,
      fret: Math.floor(i / this.numStrings)
    }
  }
}
