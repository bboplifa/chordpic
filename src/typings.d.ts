interface CSSModule {
  [className: string]: string
}

// Vexchord types
type IChord = [number, number | string, string?]
type IBarre = { fromString: number; toString: number; fret: number }
type IVexchord = { chords: IChord[]; barres: IBarre[] }

// type shims for CSS modules

declare module '*.module.scss' {
  const cssModule: CSSModule
  export = cssModule
}

declare module '*.module.css' {
  const cssModule: CSSModule
  export = cssModule
}
declare module '@rebass/forms' {
  const rebassForms = { Label: any, Input: any }
  export = rebassForms
}
declare module 'vexchords' {
  const vexchords = { ChordBox: any }
  export = vexchords
}
declare module 'react-amazing-grid'
declare module '*.svg'
declare module '*.gif'
