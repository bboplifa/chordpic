# chordpic.com | Create Chord Charts with Ease

[![Netlify Status](https://api.netlify.com/api/v1/badges/e2ca32de-59a3-4016-accd-f9c95f824d9c/deploy-status)](https://app.netlify.com/sites/romantic-joliot-44d0fc/deploys)

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

Web app I hacked together to create chord charts as easy and fast as possible. It is hosted at https://chordpic.com. There are
many solutions out there that have way more features but none is as easy and fast as this one.

## Technologies

The website is built with Gatsby and deployed with Netlify. There~~~~ is no server, everything is done in the browser.

## Contribute

Change anything you want and send me a pull request.
